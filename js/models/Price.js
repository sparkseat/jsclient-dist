(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkModel'], function(SparkModel) {
    'use strict';
    var Price;
    return Price = (function(superClass) {
      extend(Price, superClass);

      function Price() {
        return Price.__super__.constructor.apply(this, arguments);
      }

      Price.prototype.humanName = 'Price';

      return Price;

    })(SparkModel);
  });

}).call(this);
