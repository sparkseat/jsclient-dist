(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkModel'], function(SparkModel) {
    'use strict';
    var BoxOffice;
    return BoxOffice = (function(superClass) {
      extend(BoxOffice, superClass);

      function BoxOffice() {
        return BoxOffice.__super__.constructor.apply(this, arguments);
      }

      BoxOffice.prototype.humanName = 'BoxOffice';

      return BoxOffice;

    })(SparkModel);
  });

}).call(this);
