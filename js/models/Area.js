(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Function.prototype.define = function(prop, desc) {
    return Object.defineProperty(this.prototype, prop, desc);
  };

  define(['../base/SparkModel', '../collections/PriceCollection'], function(SparkModel, PriceCollection) {
    'use strict';
    var Area;
    return Area = (function(superClass) {
      extend(Area, superClass);

      function Area() {
        return Area.__super__.constructor.apply(this, arguments);
      }

      Area.prototype.humanName = 'Area';

      Area.prototype.url = function() {
        if (this.isNew()) {
          return this.collection.url;
        } else {
          return spark.api + ("/areas/" + this.id);
        }
      };

      Area.prototype.initialize = function() {
        this.prices = new PriceCollection(this.get('prices'));
        return this.prices.url = this.url() + '/prices';
      };

      return Area;

    })(SparkModel);
  });

}).call(this);
