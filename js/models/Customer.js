(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['backbone', '../base/SparkModel', '../collections/WalletCollection', '../collections/IdCardCollection'], function(Backbone, SparkModel, WalletCollection, IdCardCollection) {
    'use strict';
    var Customer;
    return Customer = (function(superClass) {
      extend(Customer, superClass);

      function Customer() {
        this.ensureEmail = bind(this.ensureEmail, this);
        return Customer.__super__.constructor.apply(this, arguments);
      }

      Customer.prototype.humanName = 'Customer';

      Customer.prototype.walletCollectionClass = WalletCollection;

      Customer.prototype.idCardCollectionClass = IdCardCollection;

      Customer.prototype.defaults = {
        first_name: "",
        last_name: "",
        organisation: "",
        email: "",
        phone_number: ""
      };

      Customer.prototype.url = function() {
        if (this.id == null) {
          return spark.api + '/customers/';
        }
        return spark.api + '/customers/' + this.id;
      };

      Customer.prototype.validate = function(attrs) {
        if (attrs.first_name.length === 0) {
          return 'You must specify a first name';
        }
        if (attrs.last_name.length === 0) {
          return 'You must specify a last name';
        }
      };

      Customer.prototype.initialize = function(details) {
        var id_cards, wallets;
        if (details != null) {
          wallets = _(details.wallet_ids).map(function(id) {
            return {
              id: id
            };
          });
          id_cards = details.id_cards;
        }
        this.wallets = new this.walletCollectionClass(wallets);
        this.idCards = new this.idCardCollectionClass(id_cards);
        this.bind('sync', this.updateUrls);
        this.updateUrls();
        return Customer.__super__.initialize.apply(this, arguments);
      };

      Customer.prototype.updateUrls = function() {
        this.wallets.url = this.url() + '/wallets';
        return this.idCards.url = this.url() + '/id_cards';
      };

      Customer.prototype.ensureEmail = function() {
        var result;
        result = $.Deferred();
        if (this.attributes.email === "") {
          result.reject();
        } else {
          result.resolve();
        }
        return result;
      };

      return Customer;

    })(SparkModel);
  });

}).call(this);
