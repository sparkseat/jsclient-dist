(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkModel'], function(SparkModel) {
    'use strict';
    var IdCard;
    return IdCard = (function(superClass) {
      extend(IdCard, superClass);

      function IdCard() {
        return IdCard.__super__.constructor.apply(this, arguments);
      }

      IdCard.prototype.humanName = 'ID Card';

      IdCard.prototype.defaults = {
        name: "",
        card_number: ""
      };

      IdCard.prototype.validate = function(attrs) {
        if (attrs.name.length === 0) {
          return 'You must specify a name';
        }
        if (attrs.card_number.length === 0) {
          return 'You must specify a card number';
        }
      };

      return IdCard;

    })(SparkModel);
  });

}).call(this);
