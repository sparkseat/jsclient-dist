(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Function.prototype.define = function(prop, desc) {
    return Object.defineProperty(this.prototype, prop, desc);
  };

  define(['../base/SparkModel', '../collections/OccurrenceCollection', '../collections/AdvisoryCollection'], function(SparkModel, OccurrenceCollection, AdvisoryCollection) {
    'use strict';
    var Event;
    return Event = (function(superClass) {
      extend(Event, superClass);

      function Event() {
        this.getRenderedDescription = bind(this.getRenderedDescription, this);
        return Event.__super__.constructor.apply(this, arguments);
      }

      Event.prototype.humanName = 'Event';

      Event.prototype.occurrenceCollectionClass = OccurrenceCollection;

      Event.prototype.advisoryCollectionClass = AdvisoryCollection;

      Event.prototype.url = function() {
        return spark.api + '/events/' + this.id;
      };

      Event.prototype.idAttribute = 'slug';

      Event.prototype.toJSON = function() {
        var json;
        json = Event.__super__.toJSON.call(this);
        json.url = this.url();
        return json;
      };

      Event.prototype.initialize = function() {
        this.occurrences = new this.occurrenceCollectionClass();
        this.occurrences.url = this.url() + '/occurrences';
        this.advisories = new this.advisoryCollectionClass();
        this.advisories.url = this.url() + '/advisories';
        Event.__super__.initialize.apply(this, arguments);
        return this.bind('sync', (function(_this) {
          return function() {
            if (_this.get('occurrences')) {
              _this.occurrences.set(_this.get('occurrences'));
            }
            if (_this.get('areas')) {
              return _this.occurrences.set(_this.get('areas'));
            }
          };
        })(this));
      };

      Event.prototype.getRenderedDescription = function() {
        if (!this.get('description')) {
          return;
        }
        return this.sync('rendered_description', this, {
          url: spark.api + '/markdown/preview',
          type: 'POST',
          data: JSON.stringify({
            markdown: this.get('description')
          })
        }).then((function(_this) {
          return function(result, status, xhr) {
            _this.set('renderedDescription', result.rendered_md);
            return _this.trigger('sync', _this, xhr, {});
          };
        })(this));
      };

      return Event;

    })(SparkModel);
  });

}).call(this);
