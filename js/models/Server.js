(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkModel'], function(SparkModel) {
    'use strict';
    var Server;
    return Server = (function(superClass) {
      extend(Server, superClass);

      function Server() {
        return Server.__super__.constructor.apply(this, arguments);
      }

      Server.prototype.humanName = 'Server';

      Server.prototype.url = function() {
        return spark.api + '/server';
      };

      return Server;

    })(SparkModel);
  });

}).call(this);
