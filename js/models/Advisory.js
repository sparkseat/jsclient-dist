(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkModel'], function(SparkModel) {
    'use strict';
    var Advisory;
    return Advisory = (function(superClass) {
      extend(Advisory, superClass);

      function Advisory() {
        return Advisory.__super__.constructor.apply(this, arguments);
      }

      Advisory.prototype.humanName = 'Advisory';

      return Advisory;

    })(SparkModel);
  });

}).call(this);
