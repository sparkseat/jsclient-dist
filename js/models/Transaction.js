(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkModel'], function(SparkModel) {
    'use strict';
    var Transaction;
    return Transaction = (function(superClass) {
      extend(Transaction, superClass);

      function Transaction() {
        return Transaction.__super__.constructor.apply(this, arguments);
      }

      Transaction.prototype.humanName = 'Transaction';

      Transaction.prototype.refund = function() {
        return this.sync('delete', this).then((function(_this) {
          return function(data) {
            return _this.set(_this.parse(data));
          };
        })(this));
      };

      return Transaction;

    })(SparkModel);
  });

}).call(this);
