(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkModel'], function(SparkModel) {
    'use strict';
    var Ticket;
    return Ticket = (function(superClass) {
      extend(Ticket, superClass);

      function Ticket() {
        this.uncollect = bind(this.uncollect, this);
        this.collect = bind(this.collect, this);
        this.unadmit = bind(this.unadmit, this);
        this.admit = bind(this.admit, this);
        return Ticket.__super__.constructor.apply(this, arguments);
      }

      Ticket.prototype.humanName = 'Ticket';

      Ticket.prototype.toJSON = function() {
        var attrs;
        attrs = _.clone(this.attributes);
        attrs.uncollected = attrs.state === 'uncollected';
        attrs.collected = attrs.state === 'collected';
        attrs.admitted = attrs.state === 'admitted';
        return attrs;
      };

      Ticket.prototype.admit = function() {
        return this.sync('admit', this, {
          type: 'POST',
          url: this.url() + '/admit',
          success: (function(_this) {
            return function(ticket) {
              return _this.set(ticket);
            };
          })(this)
        });
      };

      Ticket.prototype.unadmit = function() {
        return this.sync('unadmit', this, {
          type: 'POST',
          url: this.url() + '/unadmit',
          success: (function(_this) {
            return function(ticket) {
              return _this.set(ticket);
            };
          })(this)
        });
      };

      Ticket.prototype.collect = function() {
        return this.sync('collect', this, {
          type: 'POST',
          url: this.url() + '/collect',
          success: (function(_this) {
            return function(ticket) {
              return _this.set(ticket);
            };
          })(this)
        });
      };

      Ticket.prototype.uncollect = function() {
        return this.sync('uncollect', this, {
          type: 'POST',
          url: this.url() + '/uncollect',
          success: (function(_this) {
            return function(ticket) {
              return _this.set(ticket);
            };
          })(this)
        });
      };

      return Ticket;

    })(SparkModel);
  });

}).call(this);
