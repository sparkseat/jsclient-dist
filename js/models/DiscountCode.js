(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkModel'], function(SparkModel) {
    'use strict';
    var DiscountCode;
    return DiscountCode = (function(superClass) {
      extend(DiscountCode, superClass);

      function DiscountCode() {
        return DiscountCode.__super__.constructor.apply(this, arguments);
      }

      DiscountCode.prototype.humanName = 'Discount Code';

      return DiscountCode;

    })(SparkModel);
  });

}).call(this);
