(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['jquery', 'backbone', '../loader', '../base/SparkModel', '../collections/TicketCollection', './Ticket', './Customer', '../collections/DiscountCodeCollection', '../collections/TransactionCollection', '../collections/EventCollection'], function($, Backbone, API, SparkModel, TicketCollection, Ticket, Customer, DiscountCodeCollection, TransactionCollection, EventCollection) {
    'use strict';
    var Wallet;
    return Wallet = (function(superClass) {
      extend(Wallet, superClass);

      Wallet.prototype.humanName = 'Wallet';

      Wallet.prototype.ticketCollectionClass = TicketCollection;

      Wallet.prototype.parse = function(data) {
        data.total_charges && (data.total_charges = parseFloat(data.total_charges));
        data.total_outstanding && (data.total_outstanding = parseFloat(data.total_outstanding));
        data.total_paid && (data.total_paid = parseFloat(data.total_paid));
        data.total_ticket_value && (data.total_ticket_value = parseFloat(data.total_ticket_value));
        data.total_value && (data.total_value = parseFloat(data.total_value));
        return data;
      };

      function Wallet(attrs, options) {
        if (attrs == null) {
          attrs = {};
        }
        if (options == null) {
          options = {};
        }
        this.getPostBookingNotices = bind(this.getPostBookingNotices, this);
        this.email = bind(this.email, this);
        this.persist = bind(this.persist, this);
        this.touch = bind(this.touch, this);
        this.populateGroupedTicketsCollection = bind(this.populateGroupedTicketsCollection, this);
        if (options.url != null) {
          delete options.url;
        }
        Wallet.__super__.constructor.call(this, attrs, options);
      }

      Wallet.prototype.initialize = function(options) {
        this.discountCodes = new DiscountCodeCollection();
        this.tickets = new this.ticketCollectionClass(options.tickets);
        this.transactions = new TransactionCollection();
        this.events = new EventCollection(options.events);
        this.bind('change', this.updateUrls);
        return this.updateUrls();
      };

      Wallet.prototype.updateUrls = function() {
        var url;
        url = this.url();
        if (!url) {
          return;
        }
        this.discountCodes.url = url + '/discount_codes';
        this.tickets.url = url + '/tickets';
        return this.transactions.url = url + '/transactions';
      };

      Wallet.prototype.url = function() {
        if (this.isNew() && this.collection) {
          return this.collection.url;
        } else if (this.id) {
          return spark.api + ("/customers/" + (this.get('customer_id')) + "/wallets/" + this.id);
        }
      };

      Wallet.prototype.toJSON = function() {
        var attrs;
        attrs = _.clone(this.attributes);
        attrs.persistent || (attrs.persistent = attrs['persistent?']);
        attrs.url = this.url();
        attrs.tickets = this.tickets.toJSON();
        attrs.transactions = this.transactions.toJSON();
        return attrs;
      };

      Wallet.prototype.isSold = function() {
        return this.get('total_value') > 0 && this.get('total_outstanding') === 0;
      };

      Wallet.prototype.dateCutoffApplies = function() {
        return this.tickets.any('date_cutoff_applies');
      };

      Wallet.prototype.groupedTickets = function() {
        if (this.groupedTicketsCollection != null) {
          return this.groupedTicketsCollection;
        }
        this.eventCollection = new Backbone.Collection();
        this.tickets.on('reset', this.populateGroupedTicketsCollection);
        this.tickets.on('change', this.populateGroupedTicketsCollection);
        this.tickets.on('add', this.populateGroupedTicketsCollection);
        this.tickets.on('remove', this.populateGroupedTicketsCollection);
        this.populateGroupedTicketsCollection();
        return this.eventCollection;
      };

      Wallet.prototype.populateGroupedTicketsCollection = function() {
        var events, tickets;
        tickets = this.tickets.toJSON();
        events = _(tickets).groupBy('event_name');
        events = _(events).map(function(occurrences, event_name) {
          var event_slug;
          event_slug = occurrences[0].event_slug;
          occurrences = _(occurrences).groupBy('occurrence_id');
          occurrences = _(occurrences).map(function(areas, occurrence_id) {
            var date_cutoff_applies, time, time_str;
            time = areas[0].start_time;
            time_str = areas[0].start_time_str;
            date_cutoff_applies = areas[0].date_cutoff_applies;
            areas = _(areas).groupBy('area_id');
            areas = _(areas).map(function(prices, area_id) {
              var name;
              name = tickets[0].area_name;
              prices = _(prices).groupBy('price_id');
              prices = _(prices).map(function(tickets, price_id) {
                var count, price;
                price = tickets[0].price_value;
                count = tickets.length;
                return {
                  price_id: tickets[0].price_id,
                  price_name: tickets[0].price_name,
                  price_value: price,
                  total_value: price * count,
                  count: count
                };
              });
              return {
                area_id: area_id,
                name: name,
                prices: prices
              };
            });
            return {
              occurrence_id: occurrence_id,
              time: time,
              time_str: time_str,
              date_cutoff_applies: date_cutoff_applies,
              areas: areas
            };
          });
          return {
            slug: event_slug,
            name: event_name,
            occurrences: occurrences
          };
        });
        return this.eventCollection.set(events);
      };

      Wallet.prototype.findByToken = function() {
        return this.fetch({
          url: spark.api + '/wallets/token/' + this.get('token'),
          type: 'GET'
        });
      };

      Wallet.prototype.touch = function() {
        return this.sync('touch', this, {
          type: 'POST',
          url: this.url() + '/touch'
        });
      };

      Wallet.prototype.persist = function() {
        return this.sync('persist', this, {
          type: 'POST',
          url: this.url() + '/persist'
        }).then((function(_this) {
          return function(wallet) {
            return _this.set(_this.parse(wallet));
          };
        })(this));
      };

      Wallet.prototype.email = function() {
        return this.sync('email', this, {
          type: 'POST',
          url: this.url() + '/email'
        });
      };

      Wallet.prototype.assignTickets = function(price, area, count) {
        var data, e;
        data = {
          count: count,
          area_id: area.id,
          price_id: price.id
        };
        e = {
          cancel: false
        };
        this.trigger('before_assign_tickets', e, data);
        if (e.cancel) {
          return;
        }
        return $.when(e.deferred).then((function(_this) {
          return function() {
            return _this.sync('assign_tickets', _this, {
              type: 'POST',
              url: _this.url() + '/assign_tickets',
              data: $.param(data)
            });
          };
        })(this)).then((function(_this) {
          return function(wallet) {
            _this.tickets.set(wallet.tickets);
            return _this.set(_this.parse(wallet));
          };
        })(this));
      };

      Wallet.prototype.releaseTickets = function(price_id, area_id, count) {
        return this.sync('release_tickets', this, {
          type: 'DELETE',
          url: this.url() + '/release_tickets',
          data: $.param({
            count: count,
            price_id: price_id,
            area_id: area_id
          })
        }).then((function(_this) {
          return function(wallet) {
            _this.tickets.set(wallet.tickets);
            return _this.set(_this.parse(wallet));
          };
        })(this));
      };

      Wallet.prototype.collect = function() {
        return this.sync('collect', this, {
          type: 'POST',
          url: this.url() + '/collect',
          data: $.param({
            load: 'tickets'
          })
        }).then((function(_this) {
          return function(wallet) {
            _this.tickets.set(wallet.tickets);
            return _this.set(_this.parse(wallet));
          };
        })(this));
      };

      Wallet.prototype.getPostBookingNotices = function() {
        return this.sync.call(this, 'getPostBookingNotices', this, {
          url: this.url() + '/post_booking_notices',
          type: 'GET'
        });
      };

      return Wallet;

    })(SparkModel);
  });

}).call(this);
