(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Function.prototype.define = function(prop, desc) {
    return Object.defineProperty(this.prototype, prop, desc);
  };

  define(['../loader', '../base/SparkModel', '../collections/AreaCollection', 'moment'], function(API, SparkModel, AreaCollection, moment) {
    'use strict';
    var Occurrence;
    return Occurrence = (function(superClass) {
      extend(Occurrence, superClass);

      function Occurrence() {
        this.dispose = bind(this.dispose, this);
        this.printAdmittance = bind(this.printAdmittance, this);
        return Occurrence.__super__.constructor.apply(this, arguments);
      }

      Occurrence.prototype.humanName = 'Occurrence';

      Occurrence.prototype.defaultParams = {
        visible: false
      };

      Occurrence.prototype.url = function() {
        if (this.isNew()) {
          return this.collection.url;
        } else {
          return spark.api + ("/events/" + this.attributes.event_slug + "/occurrences/" + this.id);
        }
      };

      Occurrence.prototype.initialize = function() {
        this.areas = new AreaCollection(this.get('areas'));
        this.areas.url = this.url() + '/areas';
        Occurrence.__super__.initialize.apply(this, arguments);
        return this.bind('sync', function(occurrence) {
          if (occurrence.get('areas')) {
            return this.areas.set(occurrence.get('areas'));
          }
        });
      };

      Occurrence.define('wallets', {
        get: function() {
          var WalletCollection;
          if (this._wallets != null) {
            return this._wallets;
          }
          WalletCollection = this.walletCollectionClass() || API.collection('WalletCollection');
          this._wallets = new WalletCollection();
          this._wallets.url = this.url() + '/wallets';
          return this._wallets;
        }
      });

      Occurrence.prototype.inPast = function(adjustBy, adjustByUnit) {
        if (adjustBy == null) {
          adjustBy = 0;
        }
        if (adjustByUnit == null) {
          adjustByUnit = 'hours';
        }
        return moment(this.get('start_time')) < (moment().add(adjustBy, adjustByUnit));
      };

      Occurrence.prototype.printAdmittance = function() {
        return spark.sparkcast.printing.print({
          type: 'admittance',
          id: this.id
        });
      };

      Occurrence.prototype.dispose = function() {
        return this.stopListening();
      };

      return Occurrence;

    })(SparkModel);
  });

}).call(this);
