(function() {
  define(['backbone', '../loader'], function(Backbone, API) {
    var SparkEndpoint;
    return SparkEndpoint = (function() {
      function SparkEndpoint() {}

      SparkEndpoint.sync = function(method, model, options) {
        var _beforeSend;
        if (options == null) {
          options = {};
        }
        _beforeSend = options._beforeSend;
        options.beforeSend = (function(_this) {
          return function(xhr, options) {
            xhr.backboneDetails = {
              url: options.url,
              method: method,
              model: model
            };
            return typeof _beforeSend === "function" ? _beforeSend() : void 0;
          };
        })(this);
        return Backbone.sync(method, model, options).done(function(result) {
          if (!API.version) {
            API.version = options.xhr.getResponseHeader('X-Spark-Version');
          }
          return model.lastSync = new Date();
        });
      };

      return SparkEndpoint;

    })();
  });

}).call(this);
