(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['backbone', '../API', './SparkEndpoint'], function(Backbone, API, SparkEndpoint) {
    'use strict';
    var SparkCollection;
    return SparkCollection = (function(superClass) {
      extend(SparkCollection, superClass);

      function SparkCollection() {
        this.fetch = bind(this.fetch, this);
        return SparkCollection.__super__.constructor.apply(this, arguments);
      }

      SparkCollection.prototype.sync = SparkEndpoint.sync;

      SparkCollection.prototype.modelPlural = function() {
        return this.model.name.toLowerCase() + 's';
      };

      SparkCollection.prototype.initialize = function() {
        SparkCollection.__super__.initialize.apply(this, arguments);
        return this.listenTo(this, 'error', function(model_or_collection, xhr, options) {
          var details;
          if (xhr.errorHandled || xhr.status === 403) {
            return;
          }
          xhr.errorHandled = true;
          if (API.errorHandler != null) {
            return API.errorHandler(model_or_collection, xhr, options);
          } else {
            if (typeof console === "undefined" || console === null) {
              return;
            }
            details = xhr.backboneDetails;
            return console.error("No error handler defined, and " + details.method + " failed for " + model_or_collection.humanName + ". XHR status was " + xhr.status);
          }
        });
      };

      SparkCollection.prototype.defaultParams = {};

      SparkCollection.prototype.fetch = function(options) {
        var params;
        if (options == null) {
          options = {};
        }
        params = {};
        _.extend(params, this.defaultParams, options.data);
        options.data = params;
        return SparkCollection.__super__.fetch.call(this, options);
      };

      return SparkCollection;

    })(Backbone.Collection);
  });

}).call(this);
