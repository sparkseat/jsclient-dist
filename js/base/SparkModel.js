(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty,
    slice = [].slice;

  define(['backbone', '../API', './SparkEndpoint'], function(Backbone, API, SparkEndpoint) {
    'use strict';
    var SparkModel;
    return SparkModel = (function(superClass) {
      extend(SparkModel, superClass);

      function SparkModel() {
        var args;
        args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
        this.fetch = bind(this.fetch, this);
        this.sync = SparkEndpoint.sync;
        SparkModel.__super__.constructor.apply(this, args);
      }

      SparkModel.prototype.initialize = function() {
        SparkModel.__super__.initialize.apply(this, arguments);
        return this.listenTo(this, 'error', function(model, xhr, options) {
          var details;
          if (xhr.errorHandled || xhr.status === 403) {
            return;
          }
          xhr.errorHandled = true;
          if (API.errorHandler != null) {
            return API.errorHandler(model, xhr, options);
          } else {
            if (typeof console === "undefined" || console === null) {
              return;
            }
            details = xhr.backboneDetails;
            return console.error("No error handler defined, and " + details.method + " failed for " + model.humanName + ". XHR status was " + xhr.status);
          }
        });
      };

      SparkModel.prototype.defaultParams = {};

      SparkModel.prototype.fetch = function(options) {
        var params;
        if (options == null) {
          options = {};
        }
        params = {};
        _.extend(params, this.defaultParams, options.data);
        options.data = params;
        return SparkModel.__super__.fetch.call(this, options);
      };

      SparkModel.prototype.dirty = function() {
        return true;
      };

      return SparkModel;

    })(Backbone.Model);
  });

}).call(this);
