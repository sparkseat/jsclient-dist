(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkCollection', '../models/BoxOffice'], function(SparkCollection, BoxOffice) {
    'use strict';
    var BoxOfficeCollection;
    return BoxOfficeCollection = (function(superClass) {
      extend(BoxOfficeCollection, superClass);

      function BoxOfficeCollection() {
        return BoxOfficeCollection.__super__.constructor.apply(this, arguments);
      }

      BoxOfficeCollection.prototype.humanName = 'Box Offices';

      BoxOfficeCollection.prototype.url = function() {
        return spark.api + '/box_offices';
      };

      BoxOfficeCollection.prototype.model = BoxOffice;

      return BoxOfficeCollection;

    })(SparkCollection);
  });

}).call(this);
