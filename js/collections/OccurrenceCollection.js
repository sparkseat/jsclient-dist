(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkCollection', '../models/Occurrence'], function(SparkCollection, Occurrence) {
    'use strict';
    var OccurrenceCollection;
    return OccurrenceCollection = (function(superClass) {
      extend(OccurrenceCollection, superClass);

      function OccurrenceCollection() {
        this.dispose = bind(this.dispose, this);
        return OccurrenceCollection.__super__.constructor.apply(this, arguments);
      }

      OccurrenceCollection.prototype.humanName = 'Occurrences';

      OccurrenceCollection.prototype.defaultParams = {
        visible: false
      };

      OccurrenceCollection.prototype.model = Occurrence;

      OccurrenceCollection.prototype.dispose = function() {
        return this.each(function(occurrence) {
          return occurrence.dispose();
        });
      };

      return OccurrenceCollection;

    })(SparkCollection);
  });

}).call(this);
