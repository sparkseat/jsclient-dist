(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkCollection', '../models/Event'], function(SparkCollection, Event) {
    'use strict';
    var EventCollection;
    return EventCollection = (function(superClass) {
      extend(EventCollection, superClass);

      function EventCollection() {
        return EventCollection.__super__.constructor.apply(this, arguments);
      }

      EventCollection.prototype.humanName = 'Events';

      EventCollection.prototype.defaultParams = {
        listed: false
      };

      EventCollection.prototype.url = function() {
        return spark.api + "/events";
      };

      EventCollection.prototype.model = Event;

      return EventCollection;

    })(SparkCollection);
  });

}).call(this);
