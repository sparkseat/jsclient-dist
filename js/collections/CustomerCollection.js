(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['underscore', 'backbone', '../base/SparkCollection', '../models/Customer'], function(_, Backbone, SparkCollection, Customer) {
    'use strict';
    var CustomerCollection;
    return CustomerCollection = (function(superClass) {
      extend(CustomerCollection, superClass);

      function CustomerCollection() {
        return CustomerCollection.__super__.constructor.apply(this, arguments);
      }

      CustomerCollection.prototype.humanName = 'Customers';

      CustomerCollection.prototype.url = function() {
        return spark.api + "/customers";
      };

      CustomerCollection.prototype.model = Customer;

      CustomerCollection.prototype.search = function(query) {
        var options, q;
        q = {};
        _.each(query, function(value, name) {
          return q["q[" + name + "]"] = value;
        });
        options = {
          url: this.url(),
          type: "GET",
          data: $.param(q)
        };
        return (this.sync || Backbone.sync).call(this, null, this, options);
      };

      CustomerCollection.prototype.find = function(query) {
        var deferred, request;
        deferred = $.Deferred();
        request = this.search(query);
        request.then((function(_this) {
          return function(results) {
            if (results.length === 1) {
              return deferred.resolve(new Customer(results[0]));
            } else {
              return alert('Search returned ' + results.length + ' results.');
            }
          };
        })(this));
        return deferred;
      };

      return CustomerCollection;

    })(SparkCollection);
  });

}).call(this);
