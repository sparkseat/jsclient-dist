(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['backbone', '../base/SparkCollection', '../models/Ticket'], function(Backbone, SparkCollection, Ticket) {
    'use strict';
    var TicketCollection;
    return TicketCollection = (function(superClass) {
      extend(TicketCollection, superClass);

      function TicketCollection() {
        return TicketCollection.__super__.constructor.apply(this, arguments);
      }

      TicketCollection.prototype.humanName = 'Tickets';

      TicketCollection.prototype.model = Ticket;

      return TicketCollection;

    })(SparkCollection);
  });

}).call(this);
