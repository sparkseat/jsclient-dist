(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkCollection', '../models/Wallet'], function(SparkCollection, Wallet) {
    'use strict';
    var WalletCollection;
    return WalletCollection = (function(superClass) {
      extend(WalletCollection, superClass);

      function WalletCollection() {
        return WalletCollection.__super__.constructor.apply(this, arguments);
      }

      WalletCollection.prototype.humanName = 'Wallets';

      WalletCollection.prototype.model = Wallet;

      WalletCollection.prototype.clone = function() {
        var wallets;
        wallets = WalletCollection.__super__.clone.apply(this, arguments);
        wallets.url = this.url;
        return wallets;
      };

      return WalletCollection;

    })(SparkCollection);
  });

}).call(this);
