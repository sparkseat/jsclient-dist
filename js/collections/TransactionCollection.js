(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkCollection', '../models/Transaction'], function(SparkCollection, Transaction) {
    'use strict';
    var TransactionCollection;
    return TransactionCollection = (function(superClass) {
      extend(TransactionCollection, superClass);

      function TransactionCollection() {
        return TransactionCollection.__super__.constructor.apply(this, arguments);
      }

      TransactionCollection.prototype.humanName = 'Transactions';

      TransactionCollection.prototype.model = Transaction;

      return TransactionCollection;

    })(SparkCollection);
  });

}).call(this);
