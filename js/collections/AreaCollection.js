(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkCollection', '../models/Area'], function(SparkCollection, Area) {
    'use strict';
    var AreaCollection;
    return AreaCollection = (function(superClass) {
      extend(AreaCollection, superClass);

      function AreaCollection() {
        return AreaCollection.__super__.constructor.apply(this, arguments);
      }

      AreaCollection.prototype.humanName = 'Areas';

      AreaCollection.prototype.model = Area;

      return AreaCollection;

    })(SparkCollection);
  });

}).call(this);
