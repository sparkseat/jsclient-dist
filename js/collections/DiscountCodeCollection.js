(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkCollection', '../models/DiscountCode'], function(SparkCollection, DiscountCode) {
    'use strict';
    var DiscountCodeCollection;
    return DiscountCodeCollection = (function(superClass) {
      extend(DiscountCodeCollection, superClass);

      function DiscountCodeCollection() {
        return DiscountCodeCollection.__super__.constructor.apply(this, arguments);
      }

      DiscountCodeCollection.prototype.humanName = 'Discount Codes';

      DiscountCodeCollection.prototype.model = DiscountCode;

      return DiscountCodeCollection;

    })(SparkCollection);
  });

}).call(this);
