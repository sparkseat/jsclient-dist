(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkCollection', '../models/Price'], function(SparkCollection, Price) {
    'use strict';
    var PriceCollection;
    return PriceCollection = (function(superClass) {
      extend(PriceCollection, superClass);

      function PriceCollection() {
        return PriceCollection.__super__.constructor.apply(this, arguments);
      }

      PriceCollection.prototype.humanName = 'Prices';

      PriceCollection.prototype.defaultParams = {
        listed: false
      };

      PriceCollection.prototype.model = Price;

      return PriceCollection;

    })(SparkCollection);
  });

}).call(this);
