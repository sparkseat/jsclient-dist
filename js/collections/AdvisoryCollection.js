(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['../base/SparkCollection', '../models/Advisory'], function(SparkCollection, Advisory) {
    'use strict';
    var AdvisoryCollection;
    return AdvisoryCollection = (function(superClass) {
      extend(AdvisoryCollection, superClass);

      function AdvisoryCollection() {
        return AdvisoryCollection.__super__.constructor.apply(this, arguments);
      }

      AdvisoryCollection.prototype.humanName = 'Advisories';

      AdvisoryCollection.prototype.model = Advisory;

      return AdvisoryCollection;

    })(SparkCollection);
  });

}).call(this);
