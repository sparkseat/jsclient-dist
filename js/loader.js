(function() {
  define(['./API'], function(API) {
    var app, getSingleton;
    app = null;
    getSingleton = function() {
      if (app === null) {
        app = new API;
      }
      return app;
    };
    return getSingleton();
  });

}).call(this);
