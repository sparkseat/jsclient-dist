(function() {
  define(['jquery'], function($) {
    var Endpoint, EndpointDiscovery;
    Endpoint = (function() {
      function Endpoint(url, data) {
        this.url = url;
        this.data = data;
      }

      Endpoint.prototype.service = function(service_name) {
        if (this.isFullyQualified(this.data[service_name])) {
          return this.data[service_name];
        } else {
          return "" + this.url + this.data[service_name];
        }
      };

      Endpoint.prototype.isFullyQualified = function(url) {
        return !!url.match('^(file|https?)://');
      };

      return Endpoint;

    })();
    return EndpointDiscovery = (function() {
      function EndpointDiscovery() {}

      EndpointDiscovery.prototype.findFastest = function(endpoints, callback, suffix) {
        var failedEndpoints, selector;
        suffix || (suffix = '');
        selector = $.Deferred();
        failedEndpoints = 0;
        selector.progress(function(message) {
          if (message === 'failed') {
            failedEndpoints += 1;
            if (failedEndpoints === endpoints.length) {
              return selector.reject();
            }
          }
        });
        endpoints.forEach(function(endpoint) {
          return $.ajax({
            method: 'GET',
            url: "" + endpoint + suffix,
            async: true,
            dataType: 'json',
            crossDomain: true,
            success: function(data, textStatus, jqXHR) {
              if (callback(jqXHR)) {
                return selector.resolve(new Endpoint(endpoint, data));
              } else {
                return selector.notify('failed');
              }
            },
            error: function() {
              return selector.notify('failed');
            }
          });
        });
        return selector;
      };

      return EndpointDiscovery;

    })();
  });

}).call(this);
