(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  define(['module', 'backbone', 'marionette'], function(module, Backbone, Marionette) {
    var API;
    return API = (function(superClass) {
      extend(API, superClass);

      function API() {
        return API.__super__.constructor.apply(this, arguments);
      }

      API.prototype.moduleName = module.id.replace(/\/API$/, "");

      API.prototype.collection = function(name) {
        return require(this.moduleName + "/collections/" + name);
      };

      API.prototype.model = function(name) {
        return require(this.moduleName + "/models/" + name);
      };

      return API;

    })(Marionette.Application);
  });

}).call(this);
